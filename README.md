# ASP.Net Web API AntiXss Attribute #

An AntiXss attribute for Web API request models.
It essentially runs the Microsoft AntiXss library against the input and fails validation if the result is different to the original value. 
There are ways to fine tune and relax where appropriate.

For detailed documentation including features not detailed here please visit the [Embarr Development Labs](http://embarr-development.co.uk/labs/) page.

## Examples Usage ##

	public class TestModel
    {
        [AntiXss]
        public string Name { get; set; }

        [AntiXss(allowedStrings: "<br />,<p>")]
        public string Description { get; set; }

        [AntiXss(allowedStrings: "<br />", disallowedStrings:"/, #")]
        public string NoSlashesOrHashes { get; set; }

        [AntiXss(errorMessage: "This is a custom error message")]
        public string CustomError { get; set; }

        [AntiXss(errorMessageResourceName:"TestMessage", errorMessageResourceType: typeof(TestResources))]
        public string ResourceCustomError { get; set; }
    }

## Support for other character sets ##
As it is using the System.Web.Security.AntiXss.AntiXssEncoder you can add or modify the supported characters sets as you normally would.

For example if you wanted to support Cyrillic and the default characters sets you could add something like the following to your Application_Start method in the Global.asax:

	AntiXssEncoder.MarkAsSafe(
		LowerCodeCharts.Cyrillic | LowerCodeCharts.Default, 
		LowerMidCodeCharts.None, 
		MidCodeCharts.None, 
		UpperMidCodeCharts.CyrillicExtendedA, 
		UpperCodeCharts.None);
