﻿namespace Embarr.WebAPI.AntiXss.AcceptanceTests.WebApi.Models
{
    public class ModelValidationError
    {
        public string Property { get; set; }
        public string ErrorMessage { get; set; }
    }
}