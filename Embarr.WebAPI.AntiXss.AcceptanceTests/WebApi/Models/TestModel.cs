﻿using Embarr.WebAPI.AntiXss.AcceptanceTests.WebApi.Resources;
using Embarr.WebAPI.Validation.Mappers;

namespace Embarr.WebAPI.AntiXss.AcceptanceTests.WebApi.Models
{
    public class TestModel
    {
        [AntiXss]
        public string Name { get; set; }

        [AntiXss(allowedStrings: "<br />,<p>")]
        public string Description { get; set; }

        [AntiXss(allowedStrings: "<br />", disallowedStrings:"/")]
        public string NoSlashesOrQuotes { get; set; }

        [AntiXss(errorMessage: "This is a custom error message")]
        public string CustomError { get; set; }

        [AntiXss(errorMessageResourceName:"TestMessage", errorMessageResourceType: typeof(TestResources))]
        public string ResourceCustomError { get; set; }
    }
}