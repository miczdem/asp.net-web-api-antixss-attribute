﻿using System.Web.Http;
using Embarr.WebAPI.AntiXss.AcceptanceTests.WebApi.Models;
using Embarr.WebAPI.Validation;
using Owin;

namespace Embarr.WebAPI.AntiXss.AcceptanceTests.WebApi.App_Start
{
    public class DefaultHost
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );

            RequestValidator.Create<ModelValidationError>()
                .MapPropertyName(x => x.Property)
                .FlattenErrorMessages()
                .Map(x => x.ErrorMessage).ToErrorMessage()
                .Init(config);

            appBuilder.UseWebApi(config);
        }
    }
}