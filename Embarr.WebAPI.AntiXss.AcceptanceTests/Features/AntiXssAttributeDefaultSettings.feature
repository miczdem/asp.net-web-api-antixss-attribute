﻿@DefaultHost
Feature: AntiXssAttributeDefaultSettings
	In order to validate input to disallow strings with xss attacks
	As a developer
	I would like to decorate my properties with an attribute that uses the Microsoft AntiXss library to check the input

Scenario: Property fails with default error message
	When I pass '<br />' to the Name property
	Then I receive the status code '400'
	And I receive the error message 'The Name property failed AntiXss validation'

Scenario: Property allows br and p tags
	When I pass '<br /><p>' to the Description property
	Then I receive the status code '204'

Scenario Outline: Property explicitly disallows forward slashes and double quotes
	When I pass '<input>' to the NoSlashesOrQuotes property
	Then I receive the status code '400'
	And I receive the error message 'The NoSlashesOrQuotes property failed AntiXss validation'
	Examples:
	| TestName | input |
	| Slashes  | /     |
	| Quotes   | "     |
	
Scenario: Property returns custom error message
	When I pass '<script>' to the CustomError property
	Then I receive the status code '400'
	And I receive the error message 'This is a custom error message'

Scenario: Property returns custom error message from resource file
	When I pass '<script>' to the ResourceCustomError property
	Then I receive the status code '400'
	And I receive the error message 'This is a custom error message from a resource file'
	

	
