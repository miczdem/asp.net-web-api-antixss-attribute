﻿using System;
using TechTalk.SpecFlow;

namespace Embarr.WebAPI.AntiXss.AcceptanceTests.Services
{
    public static class ScenarioContextService
    {
        public static void SaveValue<T>(T value)
        {
            var key = typeof(T).FullName;
            SaveValue(key, value);
        }

        public static void SaveValue<T>(string key, T value)
        {
            if (ScenarioContext.Current.ContainsKey(key))
            {
                ScenarioContext.Current[key] = value;
            }
            else
            {
                ScenarioContext.Current.Add(key, value);
            }
        }

        public static T GetValue<T>()
        {
            var key = typeof(T).FullName;

            return GetValue<T>(key);
        }

        public static T GetValue<T>(string key)
        {
            if (!ScenarioContext.Current.ContainsKey(key))
            {
                throw new ArgumentException("The key does not exist in the scenario context");
            }

            return ScenarioContext.Current.Get<T>(key);
        }
    }
}